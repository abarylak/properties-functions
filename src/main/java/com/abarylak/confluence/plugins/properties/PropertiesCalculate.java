package com.abarylak.confluence.plugins.properties;

import java.util.List;
import java.util.Map;

import com.abarylak.confluence.plugins.restClient.PagePropsClient;
import com.abarylak.confluence.plugins.restClient.representations.PropRestResponse;
import com.abarylak.confluence.utils.MathUtils;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.xhtml.api.XhtmlContent;

import com.abarylak.confluence.utils.MacroConstants;
import com.abarylak.confluence.utils.MacroUtils;
import com.atlassian.renderer.v2.macro.MacroException;

public class PropertiesCalculate implements MacroConstants, Macro {
//    private final String macroName;
    private final XhtmlContent xhtmlUtils;
    private final PagePropsClient pagePropsClient;

    public PropertiesCalculate(XhtmlContent xhtmlContent, PagePropsClient pagePropsClient) {
//        this.macroName = "properties-calculate";
        this.xhtmlUtils = xhtmlContent;
        this.pagePropsClient = pagePropsClient;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
        PropRestResponse propRestResponse;
        try{
//            String valueName = MacroUtils.getStringParameter(parameters, "0", true);
            String functionName = MacroUtils.getStringParameter(parameters,FUNCTION_PARAMETER,DEFAULT_FUNCTION);
            String propertyName = MacroUtils.getStringParameter(parameters,PROPERTY_PARAMETER, null);
//            String propertyValue = propertyName != null ? MacroUtils.getStringParameter(parameters, propertyName, true) : null;
            String labelName = MacroUtils.getStringParameter(parameters,LABEL_PARAMETER,true);
            String spaceName = MacroUtils.getStringParameter(parameters,KEY_PARAMETER,null);

            propRestResponse = pagePropsClient.getPageProps(labelName, spaceName, propertyName);

            List<Number> numbers = propRestResponse.getPropertyNumbersByIndex(0);

            return renderCalculation(functionName, numbers);
        }catch (Exception e){
            return MacroUtils.showRenderedExceptionString(parameters, "Properties Calculation Exception", e);
        }
//		return propRestResponse.toString();
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

    public boolean isInline(){
        return true;
    }
    protected String renderCalculation(String functionName, List<Number> values) throws MacroException {
        if (values.isEmpty()) {
            return "";
        } else {
            Number result = MathUtils.doCalculation(functionName, values);
            return MathUtils.toString(result);
        }
    }
}

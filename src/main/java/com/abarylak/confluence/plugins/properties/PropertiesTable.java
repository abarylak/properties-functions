package com.abarylak.confluence.plugins.properties;

import com.abarylak.confluence.plugins.restClient.PagePropsClient;
import com.abarylak.confluence.plugins.restClient.representations.PropRestResponse;
import com.abarylak.confluence.utils.MacroConstants;
import com.abarylak.confluence.utils.MacroUtils;
import com.abarylak.confluence.utils.MathUtils;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;

import java.math.BigDecimal;
import java.util.*;

public class PropertiesTable implements MacroConstants, Macro {
    private final String macroName;
    private final XhtmlContent xhtmlUtils;
    private final PagePropsClient pagePropsClient;

    private static final int SUMMARISE_SUM = 1;
    private static final int SUMMARIZE_COUNT = 2;
    private static final int SUMMARIZE_MAX = 3;
    private static final int SUMMARIZE_MIN = 4;
    private static final int SUMMARIZE_TEXT = 5;

    protected static final String X_KEY = "x";
    protected static final String Y_KEY = "y";
    protected static final String VALUE_KEY = "value";
    protected static final String SUM_KEY = "summarise";
    protected static final String VALUE_NAME_KEY = "value-name";

    public PropertiesTable(XhtmlContent xhtmlContent, PagePropsClient pagePropsClient) {
        this.macroName = "properties-table";
        this.xhtmlUtils = xhtmlContent;
        this.pagePropsClient = pagePropsClient;
	}

    /**
     * The Properties table macro.
     * Usage:  {property-table:x=xvalue|y=yvalue|value=value|...}
     * Where: * xvalue - The value that will be used for the x axis of the table.  This value
     * must exist in {property} tags in at least two other pages
     * * yvalue - The value that will be used for the y axis of the table.  This value
     * must exist in {property} tags in at least two other pages
     * * value - The value that will be used to populate the body of the table.  Ideally there
     * will be a value for each x-y cell on the table.   Empty cells will have a value of " ".
     * * Other parameters are the same as for the {property-report} macro.
     */
	@Override
	public String execute(Map<String, String> parameters, String bodyContent,
			ConversionContext conversionContext) throws MacroExecutionException {
        PropRestResponse propRestResponse;
        try{
            String xValue = MacroUtils.getStringParameter(parameters, X_KEY, null);
            String yValue = MacroUtils.getStringParameter(parameters, Y_KEY, null);
            String tableValue = MacroUtils.getStringParameter(parameters, VALUE_KEY, null);
            String summarise = MacroUtils.getStringParameter(parameters, SUM_KEY, "SUM");
            StringBuffer html = new StringBuffer();

//            String valueName = MacroUtils.getStringParameter(parameters, "0", true);
            String functionName = MacroUtils.getStringParameter(parameters,FUNCTION_PARAMETER,DEFAULT_FUNCTION);
            String propertyName = MacroUtils.getStringParameter(parameters,PROPERTY_PARAMETER, null);
//            String propertyValue = propertyName != null ? MacroUtils.getStringParameter(parameters, propertyName, true) : null;
            String labelName = MacroUtils.getStringParameter(parameters,LABEL_PARAMETER,true);
            String spaceName = MacroUtils.getStringParameter(parameters,KEY_PARAMETER,null);

            //Set up the summarise constant for a little optimisation to save
            //a few string comparisons.

            int sum = SUMMARISE_SUM;
            if (summarise.compareToIgnoreCase("SUM") == 0) {
                sum = SUMMARISE_SUM;
            } else if (summarise.compareToIgnoreCase("COUNT") == 0) {
                sum = SUMMARIZE_COUNT;
            } else if (summarise.compareToIgnoreCase("MAX") == 0) {
                sum = SUMMARIZE_MAX;
            } else if (summarise.compareToIgnoreCase("MIN") == 0) {
                sum = SUMMARIZE_MIN;
            } else if (summarise.compareToIgnoreCase("TEXT") == 0) {
                sum = SUMMARIZE_TEXT;
            }

            List<String> yValues = new ArrayList<String>();
            List<String> xValues = new ArrayList<String>();
            HashMap<String, HashMap<String, Object>> table = new HashMap<String, HashMap<String, Object>>();
            HashMap<String, Object> col;
            BigDecimal currentNumber;
            BigDecimal lastNumber = new BigDecimal(0);
            //Lookup all the x,y values

            propRestResponse = pagePropsClient.getPageProps(labelName, spaceName, xValue + "," + yValue + "," + tableValue);
//            String propRestJSONResponse = pagePropsClient.getPagePropsJSON(labelName,spaceName,xValue + ", " + yValue + ", " + tableValue);
//            String propRestJSONResponse = "PlaceHolder";
            List<Number> tempNumbers = propRestResponse.getPropertyNumbersByIndex(2);
            List<String> headings = propRestResponse.getHeadings();
            for(int i = 0 ; i < propRestResponse.getPropertyValues().size(); i++){
                List<Object> values = propRestResponse.getPropertyValues().get(i);
                String xVal = values.get(0).toString();
                String yVal = values.get(1).toString();
                Object currVal = values.get(2);
                Number tempNumber = tempNumbers.get(i) != null ? tempNumbers.get(i) : null;
                if(tempNumber == null ){
                    currentNumber = new BigDecimal(0);
                }else{
                    currentNumber = new BigDecimal(tempNumber.doubleValue());
                }

                if(!yValues.contains(yVal)){
                    yValues.add(yVal);
                    col = new HashMap<String, Object>();
                    table.put(yVal, col);
                }else{
                    col = table.get(yVal);
                }

                if(!xValues.contains(xVal)){
                    xValues.add(xVal);
                }

                //Setup some initial values
                if (sum != SUMMARIZE_TEXT) {
                    if (col.get(xVal) != null) {
                        lastNumber = (BigDecimal)col.get(xVal);
                    } else {
                        if (sum == SUMMARIZE_MIN) {
                            lastNumber = new BigDecimal(Integer.MAX_VALUE);
                        } else if (sum == SUMMARIZE_MAX) {
                            lastNumber = new BigDecimal(Integer.MIN_VALUE);
                        } else {
                            lastNumber = new BigDecimal(0);
                        }
                    }
                }

                switch (sum){
                    case SUMMARIZE_TEXT: {
                        col.put(xVal,currVal);
                        break;
                    }
                    case SUMMARIZE_COUNT:{
                        col.put(xVal, new BigDecimal(lastNumber.intValue() + 1));
                        break;
                    }
                    case SUMMARIZE_MIN:{
                        if(lastNumber.compareTo(currentNumber) >= 0){
                            col.put(xVal,currentNumber);
                        }
                        break;
                    }
                    case SUMMARIZE_MAX:{
                        if(lastNumber.compareTo(currentNumber) < 0){
                            col.put(xVal,currentNumber);
                        }
                        break;
                    }
                    default:{ //Default is Summarize_SUM
                        col.put(xVal, new BigDecimal(currentNumber.intValue() + lastNumber.intValue()));
                    }
                }
            }

            //Sort the y axis
            try{
                Collections.sort(yValues);
            }catch (Exception e){
                //Can't Sort the table's y axis.
            }

            //Sort the x axis
            try {
                Collections.sort(xValues);
            }
            catch (Exception e) {
                //Can't sort the table's x axis.
            }

            html.append("<table class=\"confluenceTable tablesorter\">\n");
            html.append("<thead>");
            html.append("<tr class=\"sortableHeader\">");
            html.append("<th class=\"confluenceTh sortableHeader tablesorter-headerSortDown\">");
            html.append(xValue);
            html.append("</th>");
            for (int i = 0; i < xValues.size(); i++) {
                html.append("<th class='confluenceTh sortableHeader'>");
                html.append(xValues.get(i));
                html.append("</th>");
            }
            html.append("</tr>");
            html.append("</thead>");
            html.append("<tbody class = \"\">");

            for (int i = 0; i < yValues.size(); i++) {
                html.append("<tr>\n");
                html.append("<th class='confluenceTh'>");
                html.append(yValues.get(i));
                html.append("</th>");
                for (int j = 0; j < xValues.size(); j++) {
                    html.append("<td class='confluenceTd'>");
                    try {
                        col = table.get(yValues.get(i));
                        if (col != null) {
                            html.append(col.get(xValues.get(j)) != null ? col.get(xValues.get(j)) : "");
                        } else {
                            html.append(" ");
                        }
                    } catch (Exception e) {
                        html.append(" ");
                    }
                    html.append("</td>\n");
                }
                html.append("</tr>\n");
            }
            html.append("</tbody>");
            html.append("</table>\n");
//            html.append("<p>Result size: ");
//            html.append(propRestResponse.getPropertyValues().size());
//            html.append("</p>");
//            html.append("<p>xValues size: ");
//            html.append(xValues.size());
//            html.append("</p>");
//            html.append("<p>");
//            html.append(propRestJSONResponse);
//            html.append("</p>");
//            html.append("<p>xValue: ");
//            html.append(xValue);
//            html.append("</p>");
//            html.append("<p>yValue: ");
//            html.append(yValue);
//            html.append("</p>");
//            html.append("<p>tableValue: ");
//            html.append(tableValue);
//            html.append("</p>");
            return html.toString();


//            List<Number> numbers = propRestResponse.getPropertyNumbersByIndex(0);

//            return renderCalculation(functionName, numbers);
        }catch (Exception e){
            return MacroUtils.showRenderedExceptionString(parameters, "Properties Table Exception", e);
        }
//		return propRestResponse.toString();
	}

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	@Override
	public OutputType getOutputType() {
		return OutputType.INLINE;
	}

    public RenderMode getBodyRenderMode() {
        return RenderMode.NO_RENDER;
    }

    public boolean isInline(){
        return true;
    }
    protected String renderCalculation(String functionName, List<Number> values) throws MacroException {
        if (values.isEmpty()) {
            return "";
        } else {
            Number result = MathUtils.doCalculation(functionName, values);
            return MathUtils.toString(result);
        }
    }
}

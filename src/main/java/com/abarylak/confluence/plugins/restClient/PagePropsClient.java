package com.abarylak.confluence.plugins.restClient;

import com.abarylak.confluence.plugins.restClient.representations.PropRestResponse;

public interface PagePropsClient {
    PropRestResponse getPageProps(String label, String space, String propNames);
    String getPagePropsJSON(String label, String space, String propNames);
}

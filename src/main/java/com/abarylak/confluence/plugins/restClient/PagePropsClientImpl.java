package com.abarylak.confluence.plugins.restClient;

import com.abarylak.confluence.plugins.restClient.representations.PropRestResponse;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.sal.api.net.*;
import com.google.gson.Gson;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class PagePropsClientImpl implements PagePropsClient {
    private final RequestFactory<?> requestFactory;
    private SettingsManager settingsManager;
    private String responseString = "";
    private int totalPages;
    private int currentPageIndex;
    private PropRestResponse totalResponse;

    public PagePropsClientImpl(RequestFactory<?> requestFactory, SettingsManager settingsManager) {
        this.requestFactory = requestFactory;
        this.settingsManager = settingsManager;
	}

    @Override
    public PropRestResponse getPageProps(String label, String space, String propNames) {
        responseString = "";
        if(label == null){
//            return "Label value is null";
            return null;
        }
//        responseString += "Label: " + label;
        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
//        return "Got here without null pointer";
        if(baseUrl == null){
//            return "BaseURL value is null";
            return null;
        }
        // Build base URL
        String restUrl = baseUrl + "/rest/masterdetail/1.0/detailssummary?countComments=false&countLikes=false&sortBy=title&reverse=false&pageSize=50";
        restUrl = restUrl + "&label=" + label;
        try{
            restUrl += "&headings=" + URLEncoder.encode(propNames,"ISO-8859-1" );
        }catch(UnsupportedEncodingException e){
            return null;
        }
        restUrl += "&spaces=" + space;
//        restUrl = restUrl + "&label=1_0_release";
        currentPageIndex = 0;
        totalPages = 0;

        do{
            String finalRestUrl = restUrl + "&pageIndex=" + currentPageIndex;
            Request request = requestFactory.createRequest(Request.MethodType.GET,finalRestUrl);

            request.addTrustedTokenAuthentication();  // this takes username from ThreadLocal
            try{
                //            responseString = request.execute();
                request.execute(new ResponseHandler() {
                    @Override
                    public void handle(Response response) throws ResponseException {
                        //To change body of implemented methods use File | Settings | File Templates.
                        try{
                            String propsJson = IOUtils.toString(response.getResponseBodyAsStream());
                            Gson gson = new Gson();
                            if(currentPageIndex == 0){
                                totalResponse = gson.fromJson(propsJson, PropRestResponse.class);
                            }else{
                                totalResponse.addDetails(gson.fromJson(propsJson, PropRestResponse.class).getDetails());
                            }
                            totalPages = totalResponse.getTotalPages();
                            responseString = responseString.concat(propsJson + totalPages + "<br />");

                        } catch (IOException e){
                            throw new ResponseException(e);
                        }
                    }
                });
            } catch (ResponseException e){

            }
            currentPageIndex++;
        }while(currentPageIndex < totalPages);
        return totalResponse;
    }

    @Override
    public String getPagePropsJSON(String label, String space, String propNames) {
        responseString = "";
        if(label == null){
            return "Label value is null";
//            return null;
        }
        responseString += "Label: " + label;
        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
//        return "Got here without null pointer";
        if(baseUrl == null){
            return "BaseURL value is null";
//            return null;
        }
        // Build base URL
//        UriBuilder uriBuilder = UriBuilder.fromPath(baseUrl);
//        uriBuilder.path("/rest/masterdetail/1.0/detailssummary");
//        uriBuilder.queryParam("countComments","false");
//        uriBuilder.queryParam("countLikes","false");
//        uriBuilder.queryParam("sortBy","title");
//        uriBuilder.queryParam("reverse","false");
//        uriBuilder.queryParam("pageSize","50");
        String restUrl = baseUrl + "/rest/masterdetail/1.0/detailssummary?countComments=false&countLikes=false&sortBy=title&reverse=false&pageSize=50";
//        uriBuilder.queryParam("label",label);
        restUrl = restUrl + "&label=" + label;
//        uriBuilder.queryParam("headings",propNames);
        try{
            restUrl += "&headings=" + URLEncoder.encode(propNames,"UTF-8" );
        }catch(UnsupportedEncodingException e){
            return null;
        }
//        uriBuilder.queryParam("spaces",space);
        restUrl += "&spaces=" + space;
//        restUrl = uriBuilder.build().toString();
//        restUrl = restUrl + "&label=1_0_release";
        currentPageIndex = 0;
        totalPages = 0;

        do{
            String finalRestUrl = restUrl + "&pageIndex=" + currentPageIndex;
            Request request = requestFactory.createRequest(Request.MethodType.GET,finalRestUrl);

            request.addTrustedTokenAuthentication();  // this takes username from ThreadLocal
            try{
                //            responseString = request.execute();
                request.execute(new ResponseHandler() {
                    @Override
                    public void handle(Response response) throws ResponseException {
                        //To change body of implemented methods use File | Settings | File Templates.
                        try{
                            String propsJson = IOUtils.toString(response.getResponseBodyAsStream());
                            Gson gson = new Gson();
                            if(currentPageIndex == 0){
                                totalResponse = gson.fromJson(propsJson, PropRestResponse.class);
                            }else{
                                totalResponse.addDetails(gson.fromJson(propsJson, PropRestResponse.class).getDetails());
                            }
                            totalPages = totalResponse.getTotalPages();
                            responseString = responseString.concat(propsJson + totalPages + "<br />");

                        } catch (IOException e){
                            throw new ResponseException(e);
                        }
                    }
                });
            } catch (ResponseException e){

            }
            currentPageIndex++;
        }while(currentPageIndex < totalPages);
        return responseString + "/n<br />" + restUrl;
    }

    public void setSettingsManager(SettingsManager settingsManager){
        this.settingsManager = settingsManager;
    }
}

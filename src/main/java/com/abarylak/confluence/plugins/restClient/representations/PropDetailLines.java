package com.abarylak.confluence.plugins.restClient.representations;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: abarylak
 * Date: 11/8/13
 * Time: 2:46 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropDetailLines {
    private int id;
    private String title;
    private String relativeLink;
    private List<Object> details;
    private int likesCount;
    private int commentsCount;

    public String getTitle(){
        return (null != title) ? title : "";
    }

    public String getRelativeLink(){
        return (null != relativeLink) ? relativeLink : "";
    }

    public List<Object> getProperties(){
        return details;
    }

    public Object getPropertyIndex(int i){
        return (null != details.get(i)) ? details.get(i) : "";
    }
}

package com.abarylak.confluence.plugins.restClient.representations;

import com.abarylak.confluence.utils.MathUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: abarylak
 * Date: 11/8/13
 * Time: 2:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class PropRestResponse {
    private int currentPage;
    private int totalPages;
    private List<PropDetailLines> detailLines;
    private List<String> headings;

    public int getTotalPages(){
        return totalPages;
    }

    public int getCurrentPage(){
        return currentPage;
    }

    public List<String> getHeadings(){
        return headings;
    }

    public List<PropDetailLines> getDetails(){
        return detailLines;
    }

    public void addDetails(List<PropDetailLines> newDetails){
        detailLines.addAll(newDetails);
    }

    public List<String> getPropertiesNames(){
        return headings;
    }

    public List<List<Object>> getPropertyValues(){
        List<List<Object>> propertyValues = new ArrayList<List<Object>>();
        for(int i = 0; i < detailLines.size(); i++){
            propertyValues.add(detailLines.get(i).getProperties());
        }
        return propertyValues;
    }

    public List<Number> getPropertyNumbersByIndex(int i){
        List<Number> propNumbers = new ArrayList<Number>();
        for(int j=0; j < detailLines.size(); j++){
            Object value = detailLines.get(j).getPropertyIndex(i);
            if(value instanceof Number)
                propNumbers.add((Number)value);
            else if (value instanceof String)
                propNumbers.add(MathUtils.parseNumber((String)value));
            else
                propNumbers.add(null);
        }
        return propNumbers;
    }
}

/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.abarylak.confluence.utils;

import com.atlassian.confluence.core.*;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.spaces.SpaceDescription;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.BlogPost;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.user.PersonalInformation;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.user.UserManager;
import com.atlassian.user.User;
import com.atlassian.spring.container.ContainerManager;
//import com.abarylak.confluence.plugins.metadata.space.SpaceUtils;
//import com.abarylak.confluence.plugins.metadata.model.MetadataValue;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * This provides a number of useful access routines for content.
 */
public class ContentService {
	public static final String ATTACHMENT_KEY = "Attachment";
	public static final String AUTHOR_KEY = "Author";
	public static final String BLOG_POST_KEY = "Blog Post";
	public static final String COMMENT_KEY = "Comment";
	public static final String COMMENTS_KEY = "Comments";
	public static final String CREATOR_KEY = "Creator";
	public static final String DATE_KEY = "Date";
	public static final String DISPLAY_TITLE_KEY = "DisplayTitle";
	public static final String DOWNLOAD_KEY = "Download";
	public static final String EMAIL_KEY = "Email";
	public static final String EXCERPT_KEY = "Excerpt";
	public static final String EXCERPT_METADATA_KEY = "confluence.excerpt";
	public static final String FILE_SIZE_KEY = "File Size";
	public static final String FILE_TYPE_KEY = "File Type";
	public static final String LAST_CHANGED_BY_KEY = "Last Changed By";
	public static final String LAST_TIME_CHANGED_KEY = "Last Time Changed";
	public static final String PAGE_KEY = "Page";
	public static final String PARENT_KEY = "Parent";
	public static final String POSTED_KEY = "Posted";
	public static final String POSTER_KEY = "Poster";
	public static final String SIZE_KEY = "Size";
	public static final String SPACE_KEY = "Space";
	public static final String TIME_KEY = "Time";
	public static final String TIME_CREATED_KEY = "Time Created";
	public static final String TITLE_KEY = "Title";
	public static final String TYPE_KEY = "Type";
	public static final String USER_KEY = "User";
	public static final String VERSION_KEY = "Version";

	private final ContentPropertyManager contentPropertyManager;
	private final SpaceManager spaceManager;
	private final UserManager userManager;
	private final FormatSettingsManager formatSettingsManager;

	public ContentService(ContentPropertyManager contentPropertyManager,
			SpaceManager spaceManager, UserManager userManager,
			FormatSettingsManager formatSettingsManager) {
		this.contentPropertyManager = contentPropertyManager;
		this.spaceManager = spaceManager;
		this.userManager = userManager;
		this.formatSettingsManager = formatSettingsManager;
	}

	public ContentPropertyManager getContentPropertyManager() {
		return contentPropertyManager;
	}

	public SpaceManager getSpaceManager() {
		return spaceManager;
	}

	public UserManager getUserManager() {
		return userManager;
	}

	public FormatSettingsManager getFormatSettingsManager() {
		return formatSettingsManager;
	}

	/**
	 * Returns a named Confluence value from the specified content entity
	 * object.
	 * <p/>
	 * The following values are supported: Title - a link to the page Page - a
	 * synonym for title Excerpt - shows the page's excerpt Space - shows the
	 * page's space key
	 */
//	public Object getContentValue(ConfluenceEntityObject ceo, String valueName) {
//		ContentPropertyManager contentPropertyManager = getContentPropertyManager();
//		UserManager userManager = getUserManager();
//		DateFormatter dateFormatter = getDateFormatter();
//
//		if (AUTHOR_KEY.equals(valueName) || CREATOR_KEY.equals(valueName)
//				|| POSTER_KEY.equals(valueName))
//			return ContentUtils.getUserLink(ceo.getCreatorName());
//		else if (LAST_CHANGED_BY_KEY.equals(valueName))
//			return ContentUtils.getUserLink(ceo.getLastModifierName());
//		else if (LAST_TIME_CHANGED_KEY.equals(valueName))
//			return ceo.getLastModificationDate();
//		else if (DATE_KEY.equals(valueName)) {
//			Date date = ceo.getLastModificationDate();
//			return new MetadataValue(valueName, date, dateFormatter
//					.format(date));
//		} else if (TIME_KEY.equals(valueName)) {
//			Date date = ceo.getLastModificationDate();
//			return new MetadataValue(valueName, date, dateFormatter
//					.formatTime(date));
//		} else if (TIME_CREATED_KEY.equals(valueName)
//				|| POSTED_KEY.equals(valueName))
//			return ceo.getCreationDate();
//
//		if (ceo instanceof ContentEntityObject) {
//			ContentEntityObject c = (ContentEntityObject) ceo;
//			if (EXCERPT_KEY.equals(valueName))
//				return contentPropertyManager.getTextProperty(c,
//						EXCERPT_METADATA_KEY);
//		}
//
//		if (ceo instanceof Page) {
//			Page page = (Page) ceo;
//			if (TITLE_KEY.equals(valueName) || PAGE_KEY.equals(valueName)) {
//				return ContentUtils.getPageLink(page);
//			} else if (DISPLAY_TITLE_KEY.equals(valueName)){
//				return page.getTitle();
//			} else if (SPACE_KEY.equals(valueName)) {
//				Space space = page.getSpace();
//				return ContentUtils.getSpaceLink(space);
//			} else if (PARENT_KEY.equals(valueName)) {
//				Page parent = page.getParent();
//				if (parent != null)
//					return ContentUtils.getPageLink(parent);
//			}
//		} else if (ceo instanceof BlogPost) {
//			BlogPost blogPost = (BlogPost) ceo;
//			if (TITLE_KEY.equals(valueName) || BLOG_POST_KEY.equals(valueName)) {
//				return ContentUtils.getBlogPostLink(blogPost);
//			} else if (DISPLAY_TITLE_KEY.equals(valueName)){
//				return blogPost.getTitle();
//			} else if (SPACE_KEY.equals(valueName)) {
//				Space space = blogPost.getSpace();
//				return ContentUtils.getSpaceLink(space);
//			} else if (COMMENTS_KEY.equals(valueName)) {
//				int count = blogPost.getComments().size();
//				if (count == 0) {
//					String url = blogPost.getUrlPath()
//							+ "?showComments=true#comments";
//					return ContentUtils.getRelativeLink("0 comments", url);
//				} else {
//					Comment firstComment = (Comment) blogPost.getComments()
//							.get(0);
//					String title = count + " comment" + (count == 1 ? "" : "s");
//					return ContentUtils.getCommentLink(title, firstComment);
//				}
//			}
//		} else if (ceo instanceof SpaceDescription) {
//			SpaceDescription spaceDescription = (SpaceDescription) ceo;
//			Space space = spaceDescription.getSpace();
//			if (SPACE_KEY.equals(valueName) || TITLE_KEY.equals(valueName)) {
//				return ContentUtils.getSpaceLink(space);
//			} else if (PARENT_KEY.equals(valueName)) {
//				Space parentSpace = SpaceUtils.getParentSpace(this, space);
//				return ContentUtils.getSpaceLink(parentSpace);
//			} else if (USER_KEY.equals(valueName)) {
//				return ContentUtils.getUserLink(ceo.getCreatorName());
//			} else if (EMAIL_KEY.equals(valueName) && space.isPersonal()) {
//				User user = SpaceUtils
//						.getPersonalSpaceOwner(userManager, space);
//				if (user != null)
//					return ContentUtils.getEmailLink(user);
//			} else if (LAST_TIME_CHANGED_KEY.equals(valueName)) {
//				return space.getLastModificationDate();
//			}
//		} else if (ceo instanceof PersonalInformation) {
//			PersonalInformation personalInformation = (PersonalInformation) ceo;
//			if (USER_KEY.equals(valueName))
//				return ContentUtils.getUserLink(personalInformation
//						.getUsername());
//			else if (EMAIL_KEY.equals(valueName)) {
//				String email = personalInformation.getEmail();
//				if (!StringUtils.isBlank(email))
//					return ContentUtils.getEmailLink(email);
//			}
//		} else if (ceo instanceof Attachment) {
//			Attachment attachment = (Attachment) ceo;
//			Integer version = attachment.getAttachmentVersion();
//			if (ATTACHMENT_KEY.equals(valueName))
//				return ContentUtils.getAttachmentLink(attachment);
//			else if (TITLE_KEY.equals(valueName)
//					|| COMMENT_KEY.equals(valueName))
//				return ContentUtils.getAttachmentLink(attachment, attachment
//						.getComment());
//			else if (FILE_SIZE_KEY.equals(valueName)
//					|| SIZE_KEY.equals(valueName))
//				return new MetadataValue(valueName, new Long(attachment
//						.getFileSize()), attachment.getNiceFileSize());
//			else if (FILE_TYPE_KEY.equals(valueName)
//					|| TYPE_KEY.equals(valueName))
//				return attachment.getNiceType();
//			else if (DOWNLOAD_KEY.equals(valueName))
//				return ContentUtils.getAttachmentLink(attachment, attachment
//						.getDisplayTitle());
//			else if (VERSION_KEY.equals(valueName))
//				return ContentUtils.getAttachmentLink(attachment, attachment
//						.getDisplayTitle())
//						+ " (" + version + ")";
//		}
//		return null;
//	}

//	public MetadataValue createMetadataValue(String name, Object value) {
//		return createMetadataValue(name, value, null);
//	}
//
//	public MetadataValue createMetadataValue(String name, Object value,
//			Object defaultValue) {
//		if (value == null) {
//			if (defaultValue == null)
//				return null;
//			value = defaultValue;
//		}
//		return new MetadataValue(name, value, createWikiSnippet(value));
//	}

	/**
	 * Returns the Wiki snippet to use to represent the specified value.
	 */
	public String createWikiSnippet(Object value) {
		if (value instanceof Date)
			return getDateForUser((Date) value);
		return value != null ? value.toString() : "";
	}

	public DateFormatter getDateFormatter() {
		UserAccessor userAccessor = (UserAccessor) ContainerManager
				.getComponent("userAccessor");
		User user = AuthenticatedUserThreadLocal.getUser();
		FormatSettingsManager formatSettingsManager = getFormatSettingsManager();
		return userAccessor.getConfluenceUserPreferences(user)
				.getDateFormatter(formatSettingsManager);
	}

	public String getDateForUser(Date date) {
		DateFormatter dateFormatter = getDateFormatter();
		return dateFormatter.formatDateTime(date);
	}
}

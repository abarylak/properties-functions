/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.abarylak.confluence.utils;

import com.atlassian.confluence.pages.*;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.renderer.RenderContext;
import com.atlassian.user.User;

import org.apache.commons.lang.StringUtils;

/**
 * Useful macros to help manage Confluence content.
 */
public class ContentUtils {
	/** Returns a Wiki link to the specified page. */
	public static String getPageLink(Page page) {
		return String.format("[%s:%s]", page.getSpaceKey(), page.getTitle());
	}

	/** Returns a Wiki link to the specified page. */
	public static String getPageLink(String title, Page page) {
		return "[" + title + "|" + page.getSpaceKey() + ":" + title + "]";
	}

	/** Returns a relative link to another URL within the same Confluence site. */
	@Deprecated
	public static String getRelativeLink(String title, String urlPath) {
		return "{relative-link:" + title + "}" + urlPath + "{relative-link}";
	}

	/** Returns a Wiki link to the specified blog post. */
	public static String getBlogPostLink(BlogPost blogPost) {
		return String.format("[%s:/%s/%s]", blogPost.getSpaceKey(), blogPost.getDatePath(), blogPost.getTitle());
	}

	/** Returns a Wiki link to the specified comment. */
	public static String getCommentLink(String title, Comment comment) {
		return getRelativeLink(title, comment.getUrlPath());
	}

	/** Returns a Wiki link to the specified attachment. */
	public static String getAttachmentLink(Attachment attachment) {
		String wikiLink = String.format("[%s:%s^%s]", attachment.getSpace().getKey(), attachment.getContent().getTitle(), attachment.getFileName());
		return wikiLink;
	}

	/** Returns a Wiki link to the specified attachment, with a custom title. */
	public static String getAttachmentLink(Attachment attachment, String title) {
		String wikiLink = String.format("[%s|%s:%s^%s]", title, attachment.getSpace().getKey(), attachment.getContent().getTitle(), attachment.getFileName());
		return wikiLink;
	}

	/** Returns a Wiki link to the specified space. */
	public static String getSpaceLink(Space space) {
		return "[" + space.getName() + "|" + space.getKey() + ":]";
	}

	/** Returns a Wiki link to the user's home pages. */
	public static String getUserLink(User user) {
		return getUserLink(user.getName());
	}

	/** Returns a Wiki link to the user's home pages. */
	public static String getUserLink(String userName) {
		return userName != null ? "[~" + userName + "]" : "";
	}

	/** Returns a Wiki link to the user's email address, or null if it has none. */
	public static String getEmailLink(User user) {
		String email = user.getEmail();
		return StringUtils.isBlank(email) ? null : getEmailLink(email);
	}

	/** Returns a Wiki link to the specified email address. */
	public static String getEmailLink(String email) {
		return "[mailto:" + email + "]";
	}

	/** Returns the fully qualified URL to the specified page. */
	public static String getPageURL(RenderContext pageContext, AbstractPage page) {
		String pageURL = page.getUrlPath();
		return pageContext.getSiteRoot() + pageURL;
	}

	/** Returns true if this page is the home page for a space. */
	public static boolean isHomePage(Page page) {
		return page.equals(page.getSpace().getHomePage());
	}
}

/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.abarylak.confluence.utils;

/**
 * Useful constants for use when building plugins.
 */
public interface MacroConstants {

	// Parameter names
	public static final String CLEAR_PARAMETER        = "clear";
	public static final String FUNCTION_PARAMETER     = "function";
	public static final String HIDDEN_PARAMETER       = "hidden";
	public static final String KEY_PARAMETER          = "key";
	public static final String LABELS_PARAMETER       = "labels";
	public static final String LABEL_PARAMETER        = "label";
	public static final String LINK_PARAMETER         = "link";
	public static final String MATCH_LABELS_PARAMETER = "match";
	public static final String MAX_PARAMETER          = "max";
	public static final String MAX_RESULTS_PARAMETER  = "maxResults";
	public static final String METADATA_PARAMETER     = "metadata";
	public static final String ORIENTATION_PARAMETER  = "orientation";
	public static final String PAGES_PARAMETER        = "pages";
	public static final String PAGE_PARAMETER         = "page";
	public static final String ROOTS_PARAMETER        = "roots";
	public static final String ROOT_PARAMETER         = "root";
	public static final String SCOPE_PARAMETER        = "scope";
	public static final String SORT_PARAMETER         = "sort";
	public static final String SPACE_PARAMETER        = "space";
	public static final String SPACES_PARAMETER       = "spaces";
	public static final String STYLE_PARAMETER        = "style";
	public static final String TOTALS_PARAMETER       = "totals";
	public static final String TYPE_PARAMETER         = "type";
	public static final String VALUE_NAME_PARAMETER   = "value-name";
    public static final String PROPERTY_PARAMETER     = "property";

	// Parameter values
	public static final String ANCESTORS_KEY      = "@ancestors";
	public static final String CHILDREN_KEY       = "@children";
	public static final String DESCENDENTS_KEY    = "@descendents";
	public static final String DESCENDANTS_KEY    = "@descendants";
	public static final String HORIZONTAL_KEY     = "horizontal";
	public static final String IDENTITY_KEY       = "@identity";
	public static final String LIST_STYLE_KEY     = "list";
	public static final String OL_STYLE_KEY       = "ol";
	public static final String PARENT_KEY         = "@parent";
	public static final String PERSONAL_SPACE_KEY = "personal";
	public static final String SELF_KEY           = "@self";
	public static final String TABLE_STYLE_KEY    = "table";
	public static final String UL_STYLE_KEY       = "ul";

	// Parameter default values
	public static final String DEFAULT_FUNCTION = "sum";
	public static final String DEFAULT_STYLE    = TABLE_STYLE_KEY;
	public static final int DEFAULT_MAX_RESULTS = 500;
	
	// Sort methods
	public static final String SORT_DEFAULT = "as istring";
	public static final String SORT_STRING = "as string";
	public static final String SORT_ISTRING = "as istring";
	public static final String SORT_NUMBER = "as number";
	public static final String SORT_DATE = "as date"; 
}

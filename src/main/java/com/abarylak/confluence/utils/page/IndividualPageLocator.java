/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.abarylak.confluence.utils.page;

import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.abarylak.confluence.utils.MacroConstants;

/**
 * Locates pages. Supports @self, @parent, raw page names with an optional space key.
 * 
 * @author kelsey
 *
 */
public class IndividualPageLocator {

	private final PageManager pageManager;

	public IndividualPageLocator(PageManager pageManager) {
		this.pageManager = pageManager;
	}

	public Page find(String pageName, RenderContext renderContext, String spaceKey) throws MacroException {
		Page thisPage = null;
		if (renderContext instanceof PageContext) {
			ContentEntityObject ceo = ((PageContext)renderContext).getEntity();
			if (ceo instanceof Page) {
				thisPage = (Page)ceo;
				if(spaceKey == null || "".equals(spaceKey)) {
					// use our current space
					spaceKey = thisPage.getSpaceKey();
				}
			}
		}
		if(pageName.startsWith("@")) {
			if(thisPage == null) {
				throw new MacroException("Cannot get self/parent pages from context " + renderContext);
			}
			if(MacroConstants.SELF_KEY.equalsIgnoreCase(pageName)) {
				return thisPage;
			} else if(MacroConstants.PARENT_KEY.equalsIgnoreCase(pageName)) {
				return thisPage.getParent();
			} else {
				throw new MacroException("Page Name '" + pageName + "' is not valid");
			}
		} else {
			return getPage(renderContext, pageName, spaceKey);
		}

	}

	protected Page getPage(RenderContext renderContext, String pageName, String spaceKey) throws MacroException {
		String[] pageSections = pageName.split(":");
		if (pageSections.length == 2) {
			return getPage(pageSections[0], pageSections[1]);
		} else if (renderContext instanceof PageContext) {
			return getPage(spaceKey, pageName);
		}
		throw new MacroException("No such page \"" + pageName + "\"");
	}

	protected Page getPage(String spaceKey, String pageName) throws MacroException {
		Page page = pageManager.getPage(spaceKey, pageName);
		if (page == null)
			throw new MacroException("No such page \"" + pageName + "\"");
		return page;
	}

}

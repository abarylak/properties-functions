/*
 * Copyright (c) 2006, 2007 Andy Armstrong, Kelsey Grant and other contributors.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright notice,
 *       this list of conditions and the following disclaimer in the documentation
 *       and/or other materials provided with the distribution.
 *     * The names of contributors may not
 *       be used to endorse or promote products derived from this software without
 *       specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 * ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package com.abarylak.confluence.utils.page;

import java.util.Map;

import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.macro.MacroException;
import com.abarylak.confluence.utils.MacroUtils;
import com.abarylak.confluence.utils.MacroConstants;

public class PageSearchContext implements MacroConstants {

	@SuppressWarnings("unchecked")
	private final Map parameters;

	private final LabelManager labelManager;

	private final String spaceKey;

	private final RenderContext renderContext;

	private final PageManager pageManager;

	private final PermissionManager permissionManager;


	@SuppressWarnings("unchecked")
	public PageSearchContext(Map parameters, LabelManager labelManager,
													 RenderContext renderContext, PageManager pageManager, PermissionManager permissionManager) throws MacroException {
		this.parameters = parameters;
		this.labelManager = labelManager;
		this.renderContext = renderContext;
		this.pageManager = pageManager;
		this.permissionManager = permissionManager;
		String spaceKey = getStringParameter(SPACE_PARAMETER, "");
		if (spaceKey == null)
			spaceKey = getStringParameter(KEY_PARAMETER, "");
		this.spaceKey = spaceKey;
	}

	public LabelManager getLabelManager() {
		return labelManager;
	}

	public String getSpaceKey() {
		return spaceKey;
	}

	public String getStringParameter(String name, boolean required) throws MacroException {
		return MacroUtils.getStringParameter(parameters, name, required);
	}

	public String getStringParameter(String name, String defaultValue) throws MacroException {
		return MacroUtils.getStringParameter(parameters, name, defaultValue);
	}

	public RenderContext getRenderContext() {
		return renderContext;
	}

	public PageManager getPageManager() {
		return pageManager;
	}

	public boolean hasParameter(String key) {
		return parameters.containsKey(key);
	}

	@SuppressWarnings("unchecked")
	void setParameter(String key, String value) {
		parameters.put(key, value);
	}

	public PermissionManager getPermissionManager() {
		return permissionManager;
	}

}
